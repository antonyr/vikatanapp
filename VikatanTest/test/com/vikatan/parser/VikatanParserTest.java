package com.vikatan.parser;

import com.vikatan.utils.LoginHelper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.jsoup.nodes.Document;
import org.junit.Test;

import static org.jsoup.Jsoup.parse;

public class VikatanParserTest {
    @Test
    public void testShouldCleanupDocument() throws Exception {
        LoginHelper loginHelper = new LoginHelper("antony.java@gmail.com", "antonix1");
        DefaultHttpClient httpClient = new DefaultHttpClient();
        loginHelper.authenticate(httpClient);
        HttpGet httpGet = new HttpGet("http://www.vikatan.com/article.php?aid=13617&sid=366&mid=1&uid=463265");
        HttpResponse response = httpClient.execute(httpGet);
        HttpEntity entity = response.getEntity();
        Document document = parse(entity.getContent(), "UTF-8", "http://www.vikatan.com");
        String cleanedString = VikatanParser.cleanup(document);
        System.out.println("cleanedString = " + cleanedString);
    }
}
