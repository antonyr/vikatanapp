package com.vikatan.activity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.vikatan.activity.adapter.VikatanListAdapter;
import com.vikatan.activity.listener.VikatanHomeItemListener;
import com.vikatan.db.VikatanContent;
import com.vikatan.service.VikatanContentFetchService;
import com.vikatan.service.VikatanTitleAsyncFetchService;
import com.vikatan.utils.UnicodeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;

import java.util.Calendar;
import java.util.List;

import static com.vikatan.utils.ApplicationUtils.createCustomTypeFace;

public class AnandhaVikatanActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vikatan_list_view);
        populateApplicationTitle();
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Loading menu... Please wait.");
        dialog.setIndeterminate(true);

        boolean isTitleAvailable = checkDatabaseWhetherToPopulate(dialog);
        setupDialogDismissListener(dialog);

        if (isTitleAvailable) populateMenus();

    }

    private void populateApplicationTitle() {
        TextView titleView = (TextView) findViewById(R.id.contentViewTitle);
        titleView.setText(UnicodeUtils.unicode2tsc(getResources().getString(R.string.aanandha_vikatan)));
        titleView.setTypeface(createCustomTypeFace(this), Typeface.BOLD);
        titleView.setTextSize(18);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, Preferences.class));
                return true;
            case R.id.updateContent:
                updateContent();
                return true;
            case R.id.quit:
                this.finish();
                return true;
            case R.id.check_vikatan:
                setOffAlarm();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void populateMenus() {
        VikatanContent vikatanContent = new VikatanContent(this);
        ListView listView = (ListView) findViewById(R.id.list_view);
        listView.setAdapter(new VikatanListAdapter<NameValuePair>(this, R.layout.list_view, vikatanContent.titles()));
        listView.setOnItemClickListener(new VikatanHomeItemListener(this));
    }

    private void setupDialogDismissListener(ProgressDialog dialog) {
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                populateMenus();
            }
        });
    }

    private void setOffAlarm() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 1);
        AlarmManager alarmService = (AlarmManager) getSystemService(ALARM_SERVICE);
        PendingIntent operation = PendingIntent.getBroadcast(this, 2323, new Intent(this, VikatanNewEditionChecker.class), PendingIntent.FLAG_UPDATE_CURRENT);
        alarmService.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 1000 * 60 * 60 * 24, operation);
        createToast("Checking for new edition successfully scheduled !!!");
    }

    private void createToast(String toastMessage) {
        Toast.makeText(this, toastMessage, Toast.LENGTH_SHORT).show();
    }

    private void updateContent() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String username = preferences.getString("username", StringUtils.EMPTY);
        String password = preferences.getString("password", StringUtils.EMPTY);
        if (username.isEmpty() || password.isEmpty()) {
            Intent preferenceIntent = new Intent(this, Preferences.class);
            startActivity(preferenceIntent);
        } else {
            Intent contentFetchIntent = new Intent(this, VikatanContentFetchService.class);
            startService(contentFetchIntent);
        }
    }

    private boolean checkDatabaseWhetherToPopulate(ProgressDialog dialog) {
        VikatanContent vikatanContent = new VikatanContent(this);
        List<NameValuePair> titles = vikatanContent.titles();
        boolean isTitleAvailable = !(titles.size() == 0);
        if (!isTitleAvailable) {
            fetchTitles(dialog);
        }
        return isTitleAvailable;
    }

    @SuppressWarnings("unchecked")
    private void fetchTitles(ProgressDialog dialog) {
        new VikatanTitleAsyncFetchService(this, dialog).execute();
    }

}