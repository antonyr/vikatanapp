package com.vikatan.activity;

import android.content.Context;
import android.webkit.WebSettings;
import android.webkit.WebView;


public class CustomWebView extends WebView {

    public CustomWebView(Context context) {
        super(context);
        initializeOptions();
    }


    public void initializeOptions() {
        WebSettings settings = getSettings();
        if (settings != null) {
            settings.setJavaScriptEnabled(true);
            settings.setLoadsImagesAutomatically(true);
            settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
            settings.setSupportZoom(true);
        }
    }

}
