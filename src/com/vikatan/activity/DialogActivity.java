package com.vikatan.activity;


import android.app.Activity;
import android.app.NotificationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.vikatan.activity.listener.UpdateVikatanActivityListener;

public class DialogActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_update);

        cancelNotificationService();
        final DialogActivity activity = this;
        setupButtonListeners(activity);

    }

    private void cancelNotificationService() {
        NotificationManager service = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        service.cancel(getResources().getInteger(R.integer.vikatan_checker));
    }

    private void setupButtonListeners(final DialogActivity activity) {
        Button noButton = (Button) findViewById(R.id.update_no);
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });

        Button yesButton = (Button) findViewById(R.id.update_yes);
        yesButton.setOnClickListener(new UpdateVikatanActivityListener(this, getIntent().getExtras()));
    }
}
