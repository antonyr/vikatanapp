package com.vikatan.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import com.vikatan.activity.adapter.VikatanSubContentAdapter;
import com.vikatan.db.VikatanContent;
import com.vikatan.utils.ApplicationUtils;
import com.vikatan.utils.UnicodeUtils;
import org.apache.http.NameValuePair;

import java.util.List;

public class SubContentActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vikatan_sub_list_view);
        List<NameValuePair> subMenus = getSubmenus();
        ListView listView = (ListView) findViewById(R.id.list_view);
        populateHeaderWithCustomText();
        listView.setAdapter(new VikatanSubContentAdapter<NameValuePair>(this, R.layout.sub_list_view, subMenus));
        listView.setOnItemClickListener(new SubMenuClickListener(this));
    }

    private List<NameValuePair> getSubmenus() {
        VikatanContent vikatanContent = new VikatanContent(this);
        return vikatanContent.subtitlesByTitle(getSelectedTitle());
    }

    private void populateHeaderWithCustomText() {
        TextView submenuHeader = (TextView) findViewById(R.id.contentViewTitle);
        submenuHeader.setTypeface(ApplicationUtils.createCustomTypeFace(this), Typeface.BOLD_ITALIC);
        submenuHeader.setText(UnicodeUtils.unicode2tsc(getSelectedTitle()));
        submenuHeader.setTextSize(18);
    }

    private String getSelectedTitle() {
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        return extras.getString(getResources().getString(R.string.selected_title));
    }

}
