package com.vikatan.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.vikatan.activity.adapter.VikatanSubContentAdapter;
import org.apache.http.NameValuePair;

public class SubMenuClickListener implements AdapterView.OnItemClickListener {
    private SubContentActivity activity;

    public SubMenuClickListener(SubContentActivity activity) {
        this.activity = activity;
    }

    @SuppressWarnings("unchecked")
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        VikatanSubContentAdapter<NameValuePair> vikatanListAdapter = (VikatanSubContentAdapter<NameValuePair>) adapterView.getAdapter();
        NameValuePair item = vikatanListAdapter.getItem(position);

        Intent intent = new Intent(activity, VikatanContentActivity.class);
        intent.putExtra(activity.getResources().getString(R.string.selected_submenu), item.getName());
        activity.startActivity(intent);
    }
}
