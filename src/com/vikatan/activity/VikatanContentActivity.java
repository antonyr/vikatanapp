package com.vikatan.activity;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.vikatan.db.VikatanContent;
import com.vikatan.utils.ApplicationUtils;
import com.vikatan.utils.UnicodeUtils;

import static com.vikatan.utils.ApplicationUtils.getFooterHtml;
import static com.vikatan.utils.ApplicationUtils.getHeaderHtml;

public class VikatanContentActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vikatan_web_view);

        String content = getContent();
        Resources resources = createTitle();

        CustomWebView webView = new CustomWebView(this);
        webView.loadDataWithBaseURL(resources.getString(R.string.vikatan_base_url), getHeaderHtml(this) + UnicodeUtils.toMocha(content) + getFooterHtml(this),
                resources.getString(R.string.html_mime), resources.getString(R.string.default_encoding), null);

        LinearLayout layout = (LinearLayout) findViewById(R.id.scroller);
        layout.addView(webView);

        goFullScreen();
    }

    private Resources createTitle() {
        String subMenu = getSelectedSubmenu();
        TextView title = (TextView) findViewById(R.id.contentViewTitle);
        title.setText(UnicodeUtils.unicode2tsc(subMenu));
        Resources resources = getResources();
        title.setTypeface(ApplicationUtils.createCustomTypeFace(this), Typeface.BOLD);
        title.setTextSize(18);
        return resources;
    }

    private String getSelectedSubmenu() {
        return getIntent().getStringExtra(getResources().getString(R.string.selected_submenu));
    }

    private String getContent() {
        VikatanContent vikatanContent = new VikatanContent(this);
        return vikatanContent.findContentBySubTitle(getSelectedSubmenu());
    }

    private void goFullScreen() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
    }
}
