package com.vikatan.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.vikatan.db.VikatanMetaContent;
import com.vikatan.service.VikatanCoverImageService;
import com.vikatan.utils.UnicodeUtils;

import static com.vikatan.utils.ApplicationUtils.createCustomTypeFace;

public class VikatanHomePageActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vikatan_home);
        populateMagazineTitle();
        ImageView vikatanImage = (ImageView) findViewById(R.id.aanandha_vikatan_image);
        new VikatanCoverImageService(this, vikatanImage).execute();

        TableRow vikatanRow = (TableRow) findViewById(R.id.aanandha_vikatan_row);
        final Context outer = this;
        vikatanRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(outer, AnandhaVikatanActivity.class));
            }
        });
//        AdView adView = (AdView) findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest();
//        adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
//        adView.loadAd(adRequest);
//        adRequest.addTestDevice(AdRequest.);
    }

    private void populateMagazineTitle() {
        TextView titleView = (TextView) findViewById(R.id.aanandha_vikatan);
        titleView.setText(UnicodeUtils.unicode2tsc(getResources().getString(R.string.aanandha_vikatan)));
        titleView.setTypeface(createCustomTypeFace(this), Typeface.BOLD);
        titleView.setTextSize(18);
    }

}
