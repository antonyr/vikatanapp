package com.vikatan.activity;


import java.util.concurrent.ExecutionException;

import org.jsoup.nodes.Document;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;

import com.vikatan.db.VikatanMetaContent;
import com.vikatan.service.UrlFetcherService;
import com.vikatan.utils.ApplicationUtils;

public class VikatanNewEditionChecker extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!ApplicationUtils.isConnectivityAvailable(context)) return;

        Resources resources = context.getResources();
        String vikatanMagazine = resources.getString(R.string.vikatan_magazine);
        String oldUrl = getOldUrl(context, vikatanMagazine);
        String latestVikatanUrl = getLatestVikatanUrl(context);

        if (oldUrl == null || !oldUrl.equals(latestVikatanUrl)) {
            createNotification(context, resources, vikatanMagazine, latestVikatanUrl);
        }
    }

    private String getLatestVikatanUrl(Context context) {
        Document document;
        AsyncTask<Resources,Void,Document> task = new UrlFetcherService().execute(context.getResources());
        try {
            document = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
        return ApplicationUtils.getLatestVikatanUrl(document);
    }

    private String getOldUrl(Context context, String vikatanMagazine) {
        VikatanMetaContent metaReadonly = new VikatanMetaContent(context);
        return metaReadonly.getUrl(vikatanMagazine);
    }

    private void createNotification(Context context, Resources resources, String vikatanMagazine, String latestVikatanUrl) {
        NotificationManager systemService = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String contentText = "New Aanandha Vikatan Available";
        Notification notification = new Notification(R.drawable.vikatan_logo, contentText, System.currentTimeMillis());
        Intent dialogIntent = new Intent(context, DialogActivity.class);
        dialogIntent.putExtra(resources.getString(R.string.magazine_name), vikatanMagazine);
        dialogIntent.putExtra(resources.getString(R.string.url), latestVikatanUrl);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, dialogIntent, 0);
        notification.setLatestEventInfo(context, "Aanandha Vikatan", contentText, contentIntent);
        systemService.notify(resources.getInteger(R.integer.vikatan_checker), notification);
    }
}
