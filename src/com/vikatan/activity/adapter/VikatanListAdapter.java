package com.vikatan.activity.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.vikatan.utils.ApplicationUtils;
import com.vikatan.utils.UnicodeUtils;
import org.apache.http.NameValuePair;

import java.util.List;

public class VikatanListAdapter<T> extends ArrayAdapter<T> {
    private Context context;
    private List<NameValuePair> nameValuePairs;

    @SuppressWarnings("unchecked")
    public VikatanListAdapter(Context context, int resourceId, List<NameValuePair> nameValuePairs) {
        super(context, resourceId, (List<T>) nameValuePairs);
        this.context = context;
        this.nameValuePairs = nameValuePairs;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setTypeface(ApplicationUtils.createCustomTypeFace(context), Typeface.BOLD);
        view.setTextSize(18);
        String text = nameValuePairs.get(position).getValue();
        view.setText(UnicodeUtils.unicode2tsc(text));
        return view;
    }
}
