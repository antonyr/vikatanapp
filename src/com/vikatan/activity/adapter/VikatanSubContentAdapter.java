package com.vikatan.activity.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.vikatan.activity.R;
import com.vikatan.utils.ApplicationUtils;
import com.vikatan.utils.UnicodeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;

import java.util.List;

public class VikatanSubContentAdapter<T> extends ArrayAdapter<T> {
    private Context context;
    private List<NameValuePair> nameValuePairs;

    @SuppressWarnings("unchecked")
    public VikatanSubContentAdapter(Context context, int resourceId, List<NameValuePair> nameValuePairs) {
        super(context, resourceId, (List<T>) nameValuePairs);
        this.context = context;
        this.nameValuePairs = nameValuePairs;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.sub_list_view, null);
        populateSubtitles(position, layout);
        populateAuthor(position, layout);
        return layout;
    }

    private void populateAuthor(int position, LinearLayout layout) {
        String author = nameValuePairs.get(position).getValue();
        TextView authorView = (TextView) layout.findViewById(R.id.author);
        if (author == null || author.equals(StringUtils.EMPTY)) {
            authorView.setVisibility(View.GONE);
        }
        authorView.setTypeface(ApplicationUtils.createCustomTypeFace(context), Typeface.ITALIC);
        authorView.setTextSize(12);
        authorView.setText(UnicodeUtils.unicode2tsc(author));
    }

    private void populateSubtitles(int position, LinearLayout layout) {
        TextView submenuTitle = (TextView) layout.findViewById(R.id.submenu_title);
        submenuTitle.setTypeface(ApplicationUtils.createCustomTypeFace(context), Typeface.BOLD);
        submenuTitle.setTextSize(18);
        submenuTitle.setText(UnicodeUtils.unicode2tsc(nameValuePairs.get(position).getName()));
    }
}
