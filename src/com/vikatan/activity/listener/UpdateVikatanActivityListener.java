package com.vikatan.activity.listener;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.vikatan.activity.AnandhaVikatanActivity;
import com.vikatan.activity.DialogActivity;
import com.vikatan.activity.R;
import com.vikatan.db.VikatanContent;
import com.vikatan.db.VikatanMetaContent;
import com.vikatan.service.MediaFetcherAsyncService;
import com.vikatan.service.UrlFetcherService;
import com.vikatan.utils.ApplicationUtils;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class UpdateVikatanActivityListener implements View.OnClickListener {
    private Bundle bundle;
    private DialogActivity activity;

    public UpdateVikatanActivityListener(DialogActivity activity, Bundle bundle) {
        this.activity = activity;
        this.bundle = bundle;
    }

    @Override
    public void onClick(View view) {
        try {
            try {
                updateVikatanMetaData();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            deleteVikatanDatabaseContent();
            launchVikatanHomePage();
        } catch (IOException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
    }

    private void launchVikatanHomePage() {
        activity.finish();
        Intent intent = new Intent(activity, AnandhaVikatanActivity.class);
        activity.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    private void deleteVikatanDatabaseContent() {
        VikatanContent vikatanContent = new VikatanContent(activity);
        vikatanContent.deleteContent();
    }

    private void updateVikatanMetaData() throws IOException, ExecutionException, InterruptedException {
        Resources resources = activity.getResources();
        String magazineName = bundle.getString(resources.getString(R.string.magazine_name));
        String url = bundle.getString(resources.getString(R.string.url));
        VikatanMetaContent metaContent = new VikatanMetaContent(activity);
        Document vikatanDocument = new UrlFetcherService().execute(resources).get();
        byte[] coverImage = new MediaFetcherAsyncService(vikatanDocument).execute(resources).get();
        metaContent.updateMetaData(magazineName, url, coverImage);
    }
}
