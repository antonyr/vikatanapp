package com.vikatan.activity.listener;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.vikatan.activity.AnandhaVikatanActivity;
import com.vikatan.activity.R;
import com.vikatan.activity.SubContentActivity;
import com.vikatan.activity.adapter.VikatanListAdapter;
import org.apache.http.NameValuePair;

public class VikatanHomeItemListener implements AdapterView.OnItemClickListener {

    private AnandhaVikatanActivity activity;

    public VikatanHomeItemListener(AnandhaVikatanActivity activity) {
        this.activity = activity;
    }

    @SuppressWarnings("unchecked")
    public void onItemClick(AdapterView<?> adapterView, final View
            view, int position, long l) {
        VikatanListAdapter<NameValuePair> adapter = (VikatanListAdapter<NameValuePair>) adapterView.getAdapter();
        NameValuePair item = adapter.getItem(position);
        Intent intent = new Intent(activity, SubContentActivity.class);
        intent.putExtra(activity.getResources().getString(R.string.selected_title), item.getName());
        activity.startActivity(intent);
    }
}