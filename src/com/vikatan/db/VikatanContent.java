package com.vikatan.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class VikatanContent {
    private VikatanDatabaseHelper databaseHelper;

    public VikatanContent(Context context) {
        this.databaseHelper = new VikatanDatabaseHelper(context);
    }

    public void create(String title, String link, String subTitle) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        String insertQuery = "insert into vikatan_content(title, url, subMenu) values(?, ?, ?);";
        database.execSQL(insertQuery, new String[]{title, link, subTitle});
        database.close();
    }

    public void insertContentByUrl(String content, String author, String url) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        String insertQuery = "update vikatan_content set content = ?, author = ? where url = ?;";
        database.execSQL(insertQuery, new String[]{content, author, url});
        database.close();
    }

    public void deleteContent() {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        database.execSQL("delete from vikatan_content");
        database.close();
    }

    public List<NameValuePair> titles() {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery("select distinct(title) from vikatan_content", null);
        List<NameValuePair> titleList = new ArrayList<NameValuePair>(12);
        while (cursor.moveToNext()) {
            titleList.add(new BasicNameValuePair(cursor.getString(0), cursor.getString(0)));
        }
        cursor.close();
        database.close();
        return titleList;
    }

    public List<NameValuePair> subtitlesByTitle(String title) {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery("select subMenu, author from vikatan_content where title=?", new String[]{title});
        List<NameValuePair> subTitles = new ArrayList<NameValuePair>(10);
        while (cursor.moveToNext()) {
            subTitles.add(new BasicNameValuePair(cursor.getString(0), cursor.getString(1)));
        }
        cursor.close();
        database.close();
        return subTitles;
    }


    public List<String> urls() {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery("select url from vikatan_content where content is null or content = ''", null);
        List<String> urls = new ArrayList<String>(10);
        while (cursor.moveToNext()) {
            urls.add(cursor.getString(0));
        }
        cursor.close();
        database.close();
        return urls;
    }

    public String findContentBySubTitle(String subTitle) {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery("select content from vikatan_content where subMenu=?", new String[]{subTitle});
        cursor.moveToFirst();
        String content = cursor.getString(0);
        cursor.close();
        database.close();
        return content;
    }
}
