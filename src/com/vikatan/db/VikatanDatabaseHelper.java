package com.vikatan.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class VikatanDatabaseHelper extends SQLiteOpenHelper {

    public VikatanDatabaseHelper(Context context) {
        super(context, "Vikatan", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(createTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

    private String createTable() {
        return "Create table vikatan_content(" +
                "id integer primary key autoincrement, " +
                "content text," +
                "url text," +
                "lastModified timestamp default current_timestamp," +
                "subMenu text," +
                "title text," +
                "author text" +
                ");";
    }
}
