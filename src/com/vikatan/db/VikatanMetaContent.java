package com.vikatan.db;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import org.apache.commons.lang.StringUtils;

public class VikatanMetaContent {
    private VikatanMetaDataHelper metaDataHelper;

    public VikatanMetaContent(Context context) {
        this.metaDataHelper = new VikatanMetaDataHelper(context);
    }

    public String getUrl(String magazineName) {
        SQLiteDatabase readableDatabase = metaDataHelper.getReadableDatabase();
        Cursor cursor = readableDatabase.rawQuery("select url from vikatan_meta where title = ?", new String[]{magazineName});
        if (handleNonAvailability(readableDatabase, cursor)) return StringUtils.EMPTY;

        String url = cursor.getString(0);
        closeResources(readableDatabase, cursor);
        return url;
    }

    private void closeResources(SQLiteDatabase readableDatabase, Cursor cursor) {
        cursor.close();
        readableDatabase.close();
    }

    public void updateMetaData(String magazineName, String url, byte[] iconBytes) {
        SQLiteDatabase writableDatabase = metaDataHelper.getWritableDatabase();
        writableDatabase.execSQL("delete from vikatan_meta where title = ?", new String[]{magazineName});
        writableDatabase.execSQL("insert into vikatan_meta (title, url, icon) values (?,?,?)", new Object[]{magazineName, url, iconBytes});
        writableDatabase.close();
    }

    public byte[] getIcon(String magazineName) {
        SQLiteDatabase readableDatabase = metaDataHelper.getReadableDatabase();
        Cursor cursor = readableDatabase.rawQuery("select icon from vikatan_meta where title = ?", new String[]{magazineName});
        if (handleNonAvailability(readableDatabase, cursor)) return new byte[0];
        byte[] byteArray = cursor.getBlob(0);
        closeResources(readableDatabase, cursor);
        return byteArray;
    }

    private boolean handleNonAvailability(SQLiteDatabase readableDatabase, Cursor cursor) {
        boolean couldMove = cursor.moveToFirst();
        if (!couldMove) {
            closeResources(readableDatabase, cursor);
            return true;
        }
        return false;
    }
}
