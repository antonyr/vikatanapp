package com.vikatan.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class VikatanMetaDataHelper extends SQLiteOpenHelper {

    public VikatanMetaDataHelper(Context context) {
        super(context, "vikatan_meta", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(createTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

    private String createTable() {
        return "Create table vikatan_meta(" +
                "id integer primary key autoincrement, " +
                "title text," +
                "url text," +
                "icon blob," +
                "lastModified timestamp default current_timestamp" +
                ");";
    }
}
