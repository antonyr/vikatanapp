package com.vikatan.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ContentNode implements Serializable {

    private static final long serialVersionUID = 1L;
    private String header;
    private List<ArticleContent> subHeaders;

    public ContentNode() {
    }

    public String getHeader() {
        return this.header;
    }

    public List<ArticleContent> getSubHeaders() {
        return subHeaders;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setSubHeaders(ArrayList<ArticleContent> subHeaders) {
        this.subHeaders = subHeaders;
    }


}
