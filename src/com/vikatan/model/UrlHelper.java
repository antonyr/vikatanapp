package com.vikatan.model;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;

public class UrlHelper {

    private final URL url;

    public UrlHelper(URL url) {
        this.url = url;
    }

    public String getContent() throws IOException {
        InputStream stream = null;
        Writer writer = null;
        try {
            stream = url.openStream();
            writer = new StringWriter();
            IOUtils.copy(stream, writer);
            return writer.toString();
        } finally {
            if (stream != null) {
                stream.close();
            }
            if (writer != null) {
                writer.close();
            }
        }
    }

}
