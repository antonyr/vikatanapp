package com.vikatan.parser;


import com.vikatan.model.ArticleContent;
import com.vikatan.model.ContentNode;
import com.vikatan.model.UrlHelper;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



public class VikatanParser {
    private URL url;

    public VikatanParser(String url) throws MalformedURLException {
        this.url = new URL(url);
    }

    public List<ContentNode> getVerticalMenu() throws IOException {
        ArrayList<ContentNode> verticalMenu = new ArrayList<ContentNode>();
        UrlHelper urlHelper = new UrlHelper(url);
        String cleanupContent = convertAmpersand(urlHelper.getContent());
        Document document = Jsoup.parse(cleanupContent, "http://new.vikatan.com/");
        Elements menu = document.select("#verticalmenu");
        Elements bigMenus = menu.select("a[href=#]");
        for (Object bigMenu : bigMenus) {
            Element elem = (Element) bigMenu;
            Element parentElement = elem.parent();
            Elements submenus = parentElement.select("a[href!='#']");
            Iterator<Element> submenuIterator = submenus.iterator();
            ArrayList<ArticleContent> articleContents = new ArrayList<ArticleContent>();
            while (submenuIterator.hasNext()) {
                Element element = submenuIterator.next();
                ArticleContent articleContent = new ArticleContent();
                articleContent.setTitle(element.text());
                articleContent.setLink(element.attr("href"));
                if(articleContent.getLink().equals("#"))
                    continue;
                articleContents.add(articleContent);
            }
            ContentNode contentNode = new ContentNode();
            contentNode.setHeader(elem.text());
            contentNode.setSubHeaders(articleContents);
            verticalMenu.add(contentNode);
        }
        return verticalMenu;
    }


    private String convertAmpersand(String content) {
        String spaceRemoved = StringUtils.remove(content, "&nbsp;");
        return StringUtils.replace(spaceRemoved, "&", "&amp;");
    }


    public static String cleanup(Document document) {
        return Jsoup.clean(document.select("div.content").html(), Whitelist.relaxed());
    }

    public static String extractAuthor(Document document) {
        Elements authorElement = document.select("div.subtitle1");
        return authorElement.text();
    }

    public static boolean isLoggedIn(Document document) {
        Elements loginForm = document.select("form[name=login]");
        return loginForm.size() <= 0;
    }

}
