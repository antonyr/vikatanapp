package com.vikatan.service;

import android.content.res.Resources;
import android.os.AsyncTask;
import org.jsoup.nodes.Document;

import java.io.IOException;

import static com.vikatan.utils.ApplicationUtils.getVikatanCoverImageAsByteArray;

public class MediaFetcherAsyncService extends AsyncTask<Resources, Void, byte[]>{

    private Document vikatanDocument;

    public MediaFetcherAsyncService(Document vikatanDocument) {
        this.vikatanDocument = vikatanDocument;
    }

    @Override
    protected byte[] doInBackground(Resources...resources) {
        try {
            return getVikatanCoverImageAsByteArray(vikatanDocument);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
