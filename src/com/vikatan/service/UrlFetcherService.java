package com.vikatan.service;

import android.content.res.Resources;
import android.os.AsyncTask;
import com.vikatan.utils.ApplicationUtils;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class UrlFetcherService extends AsyncTask<Resources, Void, Document>{

    @Override
    protected Document doInBackground(Resources...resources) {
        try {
            return ApplicationUtils.getAnandhaVikatanDocument(resources[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
