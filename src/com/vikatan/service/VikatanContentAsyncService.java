package com.vikatan.service;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.Toast;
import com.vikatan.activity.Preferences;
import org.apache.commons.lang.StringUtils;

import java.util.concurrent.ExecutionException;

import static android.widget.Toast.makeText;
import static com.vikatan.utils.ApplicationUtils.isConnectivityAvailable;

public class VikatanContentAsyncService extends AsyncTask {
    private VikatanContentFetchService service;

    VikatanContentAsyncService(VikatanContentFetchService vikatanContentFetchService) {
        service = vikatanContentFetchService;
    }

    private void updateContent() {
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(service);
        String username = getUsername(preferences);
        String password = getPassword(preferences);
        if (username.isEmpty() || password.isEmpty()) {
            Intent preferenceIntent = new Intent(service, Preferences.class);
            service.startActivity(preferenceIntent);
        } else {
            fetchContent(preferences);
        }
    }

    @Override
    protected void onPreExecute() {
        if (!isConnectivityAvailable(service)) {
            createToast("Please check your internet connectivity");
            cancel(true);
        }
        createToast("fetching content from www.vikatan.com");
    }

    private void fetchContent(SharedPreferences preferences) {
        String username = getUsername(preferences);
        String password = getPassword(preferences);
        VikatanContentRetriever contentRetriever = new VikatanContentRetriever(service, username, password);
        contentRetriever.execute();
    }

    private void createToast(String toastMessage) {
        makeText(service, toastMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostExecute(Object o) {
        try {
            this.get();
            createToast("successfully fetched the content !!!");
        } catch (InterruptedException e) {
            createToast("sorry, you could not be authenticated !!!!");
        } catch (ExecutionException e) {
            createToast("sorry, you could not be authenticated !!!!");
        }
    }

    private String getPassword(SharedPreferences preferences) {
        return preferences.getString("password", StringUtils.EMPTY);
    }

    private String getUsername(SharedPreferences preferences) {
        return preferences.getString("username", StringUtils.EMPTY);
    }

    @Override
    protected Object doInBackground(Object... voids) {
        updateContent();
        return null;
    }
}
