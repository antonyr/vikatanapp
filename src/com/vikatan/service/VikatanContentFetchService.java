package com.vikatan.service;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class VikatanContentFetchService extends Service {
    @Override
    @SuppressWarnings("unchecked")
    public int onStartCommand(Intent intent, int flags, int startId) {
        new VikatanContentAsyncService(this).execute();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}