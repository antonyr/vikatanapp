package com.vikatan.service;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import com.vikatan.activity.R;
import com.vikatan.db.VikatanContent;
import com.vikatan.exception.NotLoggedInException;
import com.vikatan.utils.LoginHelper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static com.vikatan.parser.VikatanParser.cleanup;
import static com.vikatan.parser.VikatanParser.extractAuthor;
import static com.vikatan.parser.VikatanParser.isLoggedIn;
import static org.jsoup.Jsoup.parse;

public class VikatanContentRetriever {
    private Context context;
    private final String username;
    private final String password;

    public VikatanContentRetriever(Context context, String username, String password) {
        this.context = context;
        this.username = username;
        this.password = password;
    }

    public void execute() {
        VikatanContent content = new VikatanContent(context);

        List<String> urls = content.urls();
        Log.i(getClass().getName(), "urls.size() = " + urls.size());

        LoginHelper loginHelper = new LoginHelper(username, password);
        DefaultHttpClient httpClient = new DefaultHttpClient();
        loginHelper.authenticate(httpClient);
        Resources resources = context.getResources();
        for (String url : urls) {
            String baseUri = resources.getString(R.string.vikatan_base_url);
            HttpGet get = new HttpGet(url);
            try {
                HttpResponse httpResponse = httpClient.execute(get);
                HttpEntity entity = httpResponse.getEntity();
                InputStream inputStream = entity.getContent();
                Document document = parse(inputStream, resources.getString(R.string.default_encoding), baseUri);
                if (!isLoggedIn(document)) {
                    throw new NotLoggedInException("Oh No, You could not log in !!!!");
                }
                String author = extractAuthor(document);
                String cleanedDocument = cleanup(document);
                Log.i(this.getClass().getSimpleName(), "author::" + author);
                Log.i(this.getClass().getSimpleName(), "cleaned document::" + cleanedDocument);
                content.insertContentByUrl(cleanedDocument, author, url);
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(getClass().getName(), e.getMessage());
            }
        }
    }
}
