package com.vikatan.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import com.vikatan.activity.R;
import com.vikatan.db.VikatanMetaContent;
import com.vikatan.utils.ApplicationUtils;
import org.jsoup.nodes.Document;

import java.io.IOException;

import static com.vikatan.utils.ApplicationUtils.getVikatanCoverImageAsByteArray;

public class VikatanCoverImageService extends AsyncTask {
    private Context context;
    private ImageView imageView;

    public VikatanCoverImageService(Context context, ImageView imageView) {
        this.context = context;
        this.imageView = imageView;
    }

    @Override
    protected void onPostExecute(Object o) {
        if(o != null){
            imageView.invalidate();
            imageView.setImageBitmap((Bitmap) o);
            imageView.setDrawingCacheEnabled(true);
        }
    }

    @Override
    protected Object doInBackground(Object... objects) {
        byte[] imageBytes = getImageBytes();
        if (imageBytes != null && imageBytes.length > 0) {
            return createBitmap(imageBytes);
        }
        try {
            if (ApplicationUtils.isConnectivityAvailable(context)) {
                Document vikatanDocument = ApplicationUtils.getAnandhaVikatanDocument(context.getResources());
                return createBitmap(getVikatanCoverImageAsByteArray(vikatanDocument));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Object createBitmap(byte[] imageBytes) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length, options);
    }

    private byte[] getImageBytes() {
        VikatanMetaContent metaContent = new VikatanMetaContent(context);
        return metaContent.getIcon(context.getResources().getString(R.string.vikatan_magazine));
    }
}
