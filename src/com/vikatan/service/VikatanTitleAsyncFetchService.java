package com.vikatan.service;

import static com.vikatan.utils.ApplicationUtils.getAnandhaVikatanDocument;
import static com.vikatan.utils.ApplicationUtils.getLatestVikatanUrl;
import static com.vikatan.utils.ApplicationUtils.isConnectivityAvailable;
import static com.vikatan.utils.UnicodeUtils.unicode2tsc;

import java.io.IOException;
import java.util.List;

import org.jsoup.nodes.Document;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.vikatan.activity.R;
import com.vikatan.db.VikatanContent;
import com.vikatan.db.VikatanMetaContent;
import com.vikatan.model.ArticleContent;
import com.vikatan.model.ContentNode;
import com.vikatan.parser.VikatanParser;
import com.vikatan.utils.ApplicationUtils;

public class VikatanTitleAsyncFetchService extends AsyncTask {
    private Context context;
    private ProgressDialog dialog;

    public VikatanTitleAsyncFetchService(Context context, ProgressDialog dialog) {
        this.context = context;
        this.dialog = dialog;
    }

    @Override
    protected void onPreExecute() {
        dialog.show();
        if (!isConnectivityAvailable(context)) {
            Toast.makeText(context, "Please check your internet connectivity", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
            cancel(true);
        }
    }

    @Override
    protected void onPostExecute(Object o) {
        dialog.dismiss();
    }

    @Override
    protected Object doInBackground(Object... objects) {
        try {
            fetchTitles();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void fetchTitles() throws IOException {
        Document document = getAnandhaVikatanDocument(context.getResources());
        String vikatanUrl = getLatestVikatanUrl(document);
        List<ContentNode> verticalMenu = getVerticalMenu(vikatanUrl);
        populateTables(verticalMenu);
        byte[] imageByteArray = ApplicationUtils.getVikatanCoverImageAsByteArray(document);
        updateVikatanMetaData(vikatanUrl, imageByteArray);
    }

    private List<ContentNode> getVerticalMenu(String vikatanUrl) throws IOException {
        VikatanParser vikatanParser = new VikatanParser(vikatanUrl);
        return vikatanParser.getVerticalMenu();
    }

    private void updateVikatanMetaData(String url, byte[] iconByteArray) {
        VikatanMetaContent metaContent = new VikatanMetaContent(context);
        metaContent.updateMetaData(context.getResources().getString(R.string.vikatan_magazine), url, iconByteArray);
    }

    private void populateTables(List<ContentNode> contentNodes) {
        VikatanContent content = new VikatanContent(context);
        for (ContentNode contentNode : contentNodes) {
            for (ArticleContent articleContent : contentNode.getSubHeaders()) {
                content.create(unicode2tsc(contentNode.getHeader()), articleContent.getLink(), unicode2tsc(articleContent.getTitle()));
            }
        }
    }
}
