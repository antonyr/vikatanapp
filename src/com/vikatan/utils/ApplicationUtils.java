package com.vikatan.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.vikatan.activity.R;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;


public class ApplicationUtils {

    private static String headerHtml;
    private static String footerHtml;

    public static Typeface createCustomTypeFace(Context context) {
        return Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.tsc_font));
    }

    public static String getHeaderHtml(Context context) {
        if (headerHtml != null) {
            return headerHtml;
        }

        Writer writer = new StringWriter();
        InputStream inputStream = context.getResources().openRawResource(R.raw.header);
        try {
            IOUtils.copy(inputStream, writer);
        } catch (IOException e) {
            return null;
        }

        headerHtml = writer.toString();
        return headerHtml;
    }

    public static String getFooterHtml(Context context) {
        if (footerHtml != null) {
            return footerHtml;
        }

        StringWriter writer = new StringWriter();
        InputStream inputStream = context.getResources().openRawResource(R.raw.footer);
        try {
            IOUtils.copy(inputStream, writer);
        } catch (IOException e) {
            return null;
        }
        footerHtml = writer.toString();
        return footerHtml;
    }

    public static boolean isConnectivityAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public static String getLatestVikatanUrl(Document doc) {
        Elements elements = doc.select("#megaanchor");
        return elements.get(0).attr("href");
    }

    public static byte[] getImageAsByteArray(String imageUrl) throws IOException {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpResponse response = httpClient.execute(new HttpGet(imageUrl));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        response.getEntity().writeTo(outputStream);
        return outputStream.toByteArray();
    }

    public static byte[] getVikatanCoverImageAsByteArray(Document document) throws IOException {
        Elements vikatanIconElement = document.select("div#megamenu1 img");
        String vikatanIconUrl = vikatanIconElement.attr("src");
        return ApplicationUtils.getImageAsByteArray(vikatanIconUrl);
    }

    public static Document getAnandhaVikatanDocument(Resources resources) throws IOException {
        return fetchDocument(resources.getString(R.string.aanandha_vikatan_url));
    }

    private static Document fetchDocument(String vikatanUrl) throws IOException {
        return Jsoup.connect(vikatanUrl).timeout(100000).get();
    }
}