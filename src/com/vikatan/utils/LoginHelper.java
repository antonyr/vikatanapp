package com.vikatan.utils;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class LoginHelper {
    private String username;
    private String password;

    public LoginHelper(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public HttpClient authenticate(DefaultHttpClient httpClient) {
        String url = "http://vikatan.com/online/";
        HttpPost request = new HttpPost(url);
        prepareHeader(request);
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try {
            prepareParameters(request, nameValuePairs);
            HttpResponse response = httpClient.execute(request);
            InputStream inputStream = response.getEntity().getContent();
            inputStream.close();
            return httpClient;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return httpClient;
    }

    private void prepareParameters(HttpPost request,
                                   List<NameValuePair> nameValuePairs)
            throws UnsupportedEncodingException {
        nameValuePairs.add(new BasicNameValuePair("user_id", username));
        nameValuePairs.add(new BasicNameValuePair("password", password));
        nameValuePairs.add(new BasicNameValuePair("remember_me", "on"));
        nameValuePairs.add(new BasicNameValuePair("act", "lin"));
        nameValuePairs.add(new BasicNameValuePair("Sign+In.x", "0"));
        nameValuePairs.add(new BasicNameValuePair("Sign+In.y", "0"));
        request.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
    }

    private void prepareHeader(HttpPost request) {
        request.addHeader("Content-Type", "application/x-www-form-urlencoded");
        request.addHeader("User-Agent",
                "Mozilla/5.0 (X11; Linux i686; rv:2.0b9) Gecko/20100101 Firefox/4.0b9");
        request.addHeader("Accept",
                "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        request.addHeader("Accept-Language", "en-us,en;q=0.5");
        request.addHeader("Accept-Encoding", "gzip, deflate");
        request.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
        request.addHeader("Referer",
                "http://new.vikatan.com/online/?error_code=907");
    }
}
