package com.vikatan.utils;

public class UnicodeUtils {

    public static String unicode2tsc(String unicodeString) {
        if (unicodeString == null) return "";
        unicodeString = unicodeString.replace("ஜௌ", "¦ƒª");
        unicodeString = unicodeString.replace("ஜோ", "§ƒ¡");
        unicodeString = unicodeString.replace("ஜொ", "¦ƒ¡");
        unicodeString = unicodeString.replace("ஜா", "ƒ¡");
        unicodeString = unicodeString.replace("ஜி", "ƒ¢");
        unicodeString = unicodeString.replace("ஜீ", "ƒ£");
        unicodeString = unicodeString.replace("ஜு", "ƒ¤");
        unicodeString = unicodeString.replace("ஜூ", "ƒ¥");
        unicodeString = unicodeString.replace("ஜெ", "¦ƒ");
        unicodeString = unicodeString.replace("ஜே", "§ƒ");
        unicodeString = unicodeString.replace("ஜை", "¨ƒ");
        unicodeString = unicodeString.replace("ஜ்", "ˆ");
        unicodeString = unicodeString.replace("ஜ", "ƒ");


        unicodeString = unicodeString.replace("கௌ", "¦¸ª");
        unicodeString = unicodeString.replace("கோ", "§¸¡");
        unicodeString = unicodeString.replace("கொ", "¦¸¡");
        unicodeString = unicodeString.replace("கா", "¸¡");
        unicodeString = unicodeString.replace("கி", "¸¢");
        unicodeString = unicodeString.replace("கீ", "¸£");
        unicodeString = unicodeString.replace("கு", "Ì");
        unicodeString = unicodeString.replace("கூ", "Ü");
        unicodeString = unicodeString.replace("கெ", "¦¸");
        unicodeString = unicodeString.replace("கே", "§¸");
        unicodeString = unicodeString.replace("கை", "¨¸");
        unicodeString = unicodeString.replace("க்", "ì");
        unicodeString = unicodeString.replace("க", "¸");


        unicodeString = unicodeString.replace("ஙௌ", "¦¹ª");
        unicodeString = unicodeString.replace("ஙோ", "§¹¡");
        unicodeString = unicodeString.replace("ஙொ", "¦¹¡");
        unicodeString = unicodeString.replace("ஙா", "¹¡");
        unicodeString = unicodeString.replace("ஙி", "¹¢");
        unicodeString = unicodeString.replace("ஙீ", "¹£");
        unicodeString = unicodeString.replace("ஙு", "");
        unicodeString = unicodeString.replace("ஙூ", "");
        unicodeString = unicodeString.replace("ஙெ", "¦¹");
        unicodeString = unicodeString.replace("ஙே", "§¹");
        unicodeString = unicodeString.replace("ஙை", "¨¹");
        unicodeString = unicodeString.replace("ங்", "í");
        unicodeString = unicodeString.replace("ங", "¹");


        unicodeString = unicodeString.replace("சௌ", "¦ºª");
        unicodeString = unicodeString.replace("சோ", "§º¡");
        unicodeString = unicodeString.replace("சொ", "¦º¡");
        unicodeString = unicodeString.replace("சா", "º¡");
        unicodeString = unicodeString.replace("சி", "º¢");
        unicodeString = unicodeString.replace("சீ", "º£");
        unicodeString = unicodeString.replace("சு", "Í");
        unicodeString = unicodeString.replace("சூ", "Ý");
        unicodeString = unicodeString.replace("செ", "¦º");
        unicodeString = unicodeString.replace("சே", "§º");
        unicodeString = unicodeString.replace("சை", "¨º");
        unicodeString = unicodeString.replace("ச்", "î");
        unicodeString = unicodeString.replace("ச", "º");


        unicodeString = unicodeString.replace("ஞௌ", "¦»ª");
        unicodeString = unicodeString.replace("ஞோ", "§»¡");
        unicodeString = unicodeString.replace("ஞொ", "¦»¡");
        unicodeString = unicodeString.replace("ஞா", "»¡");
        unicodeString = unicodeString.replace("ஞி", "»¢");
        unicodeString = unicodeString.replace("ஞீ", "»£");
        unicodeString = unicodeString.replace("ஞு", "");
        unicodeString = unicodeString.replace("ஞூ", "");
        unicodeString = unicodeString.replace("ஞெ", "¦»");
        unicodeString = unicodeString.replace("ஞே", "§»");
        unicodeString = unicodeString.replace("ஞை", "¨»");
        unicodeString = unicodeString.replace("ஞ்", "ï");
        unicodeString = unicodeString.replace("ஞ", "»");


        unicodeString = unicodeString.replace("டௌ", "¦¼ª");
        unicodeString = unicodeString.replace("டோ", "§¼¡");
        unicodeString = unicodeString.replace("டொ", "¦¼¡");
        unicodeString = unicodeString.replace("டா", "¼¡");
        unicodeString = unicodeString.replace("டி", "Ê");
        unicodeString = unicodeString.replace("டீ", "Ë");
        unicodeString = unicodeString.replace("டு", "Î");
        unicodeString = unicodeString.replace("டூ", "Þ");
        unicodeString = unicodeString.replace("டெ", "¦¼");
        unicodeString = unicodeString.replace("டே", "§¼");
        unicodeString = unicodeString.replace("டை", "¨¼");
        unicodeString = unicodeString.replace("ட்", "ð");
        unicodeString = unicodeString.replace("ட", "¼");


        unicodeString = unicodeString.replace("ணௌ", "¦½ª");
        unicodeString = unicodeString.replace("ணோ", "§½¡");
        unicodeString = unicodeString.replace("ணொ", "¦½¡");
        unicodeString = unicodeString.replace("ணா", "½¡");
        unicodeString = unicodeString.replace("ணி", "½¢");
        unicodeString = unicodeString.replace("ணீ", "½£");
        unicodeString = unicodeString.replace("ணு", "Ï");
        unicodeString = unicodeString.replace("ணூ", "ß");
        unicodeString = unicodeString.replace("ணெ", "¦½");
        unicodeString = unicodeString.replace("ணே", "§½");
        unicodeString = unicodeString.replace("ணை", "¨½");
        unicodeString = unicodeString.replace("ண்", "ñ");
        unicodeString = unicodeString.replace("ண", "½");

        unicodeString = unicodeString.replace("தௌ", "¦¾ª");
        unicodeString = unicodeString.replace("தோ", "§¾¡");
        unicodeString = unicodeString.replace("தொ", "¦¾¡");
        unicodeString = unicodeString.replace("தா", "¾¡");
        unicodeString = unicodeString.replace("தி", "¾¢");
        unicodeString = unicodeString.replace("தீ", "¾£");
        unicodeString = unicodeString.replace("து", "Ð");
        unicodeString = unicodeString.replace("தூ", "à");
        unicodeString = unicodeString.replace("தெ", "¦¾");
        unicodeString = unicodeString.replace("தே", "§¾");
        unicodeString = unicodeString.replace("தை", "¨¾");
        unicodeString = unicodeString.replace("த்", "ò");
        unicodeString = unicodeString.replace("த", "¾");


        unicodeString = unicodeString.replace("நௌ", "¦¿ª");
        unicodeString = unicodeString.replace("நோ", "§¿¡");
        unicodeString = unicodeString.replace("நொ", "¦¿¡");
        unicodeString = unicodeString.replace("நா", "¿¡");
        unicodeString = unicodeString.replace("நி", "¿¢");
        unicodeString = unicodeString.replace("நீ", "¿£");
        unicodeString = unicodeString.replace("நு", "Ñ");
        unicodeString = unicodeString.replace("நூ", "á");
        unicodeString = unicodeString.replace("நெ", "¦¿");
        unicodeString = unicodeString.replace("நே", "§¿");
        unicodeString = unicodeString.replace("நை", "¨¿");
        unicodeString = unicodeString.replace("ந்", "ó");
        unicodeString = unicodeString.replace("ந", "¿");


        unicodeString = unicodeString.replace("னௌ", "¦Éª");
        unicodeString = unicodeString.replace("னோ", "§É¡");
        unicodeString = unicodeString.replace("னொ", "¦É¡");
        unicodeString = unicodeString.replace("னா", "É¡");
        unicodeString = unicodeString.replace("னி", "É¢");
        unicodeString = unicodeString.replace("னீ", "É£");
        unicodeString = unicodeString.replace("னு", "Û");
        unicodeString = unicodeString.replace("னூ", "ë");
        unicodeString = unicodeString.replace("னெ", "¦É");
        unicodeString = unicodeString.replace("னே", "§É");
        unicodeString = unicodeString.replace("னை", "¨É");
        unicodeString = unicodeString.replace("ன்", "ý");
        unicodeString = unicodeString.replace("ன", "É");


        unicodeString = unicodeString.replace("பௌ", "¦Àª");
        unicodeString = unicodeString.replace("போ", "§À¡");
        unicodeString = unicodeString.replace("பொ", "¦À¡");
        unicodeString = unicodeString.replace("பா", "À¡");
        unicodeString = unicodeString.replace("பி", "À¢");
        unicodeString = unicodeString.replace("பீ", "À£");
        unicodeString = unicodeString.replace("பு", "Ò");
        unicodeString = unicodeString.replace("பூ", "â");
        unicodeString = unicodeString.replace("பெ", "¦À");
        unicodeString = unicodeString.replace("பே", "§À");
        unicodeString = unicodeString.replace("பை", "¨À");
        unicodeString = unicodeString.replace("ப்", "ô");
        unicodeString = unicodeString.replace("ப", "À");


        unicodeString = unicodeString.replace("மௌ", "¦Áª");
        unicodeString = unicodeString.replace("மோ", "§Á¡");
        unicodeString = unicodeString.replace("மொ", "¦Á¡");
        unicodeString = unicodeString.replace("மா", "Á¡");
        unicodeString = unicodeString.replace("மி", "Á¢");
        unicodeString = unicodeString.replace("மீ", "Á£");
        unicodeString = unicodeString.replace("மு", "Ó");
        unicodeString = unicodeString.replace("மூ", "ã");
        unicodeString = unicodeString.replace("மெ", "¦Á");
        unicodeString = unicodeString.replace("மே", "§Á");
        unicodeString = unicodeString.replace("மை", "¨Á");
        unicodeString = unicodeString.replace("ம்", "õ");
        unicodeString = unicodeString.replace("ம", "Á");


        unicodeString = unicodeString.replace("யௌ", "¦Âª");
        unicodeString = unicodeString.replace("யோ", "§Â¡");
        unicodeString = unicodeString.replace("யொ", "¦Â¡");
        unicodeString = unicodeString.replace("யா", "Â¡");
        unicodeString = unicodeString.replace("யி", "Â¢");
        unicodeString = unicodeString.replace("யீ", "Â£");
        unicodeString = unicodeString.replace("யு", "Ô");
        unicodeString = unicodeString.replace("யூ", "ä");
        unicodeString = unicodeString.replace("யெ", "¦Â");
        unicodeString = unicodeString.replace("யே", "§Â");
        unicodeString = unicodeString.replace("யை", "¨Â");
        unicodeString = unicodeString.replace("ய்", "ö");
        unicodeString = unicodeString.replace("ய", "Â");

        unicodeString = unicodeString.replace("ரௌ", "¦Ãª");
        unicodeString = unicodeString.replace("ரோ", "§Ã¡");
        unicodeString = unicodeString.replace("ரொ", "¦Ã¡");
        unicodeString = unicodeString.replace("ரா", "Ã¡");
        unicodeString = unicodeString.replace("ரி", "Ã¢");
        unicodeString = unicodeString.replace("ரீ", "Ã£");
        unicodeString = unicodeString.replace("ரு", "Õ");
        unicodeString = unicodeString.replace("ரூ", "å");
        unicodeString = unicodeString.replace("ரெ", "¦Ã");
        unicodeString = unicodeString.replace("ரே", "§Ã");
        unicodeString = unicodeString.replace("ரை", "¨Ã");
        unicodeString = unicodeString.replace("ர்", "÷");
        unicodeString = unicodeString.replace("ர", "Ã");


        unicodeString = unicodeString.replace("லௌ", "¦Äª");
        unicodeString = unicodeString.replace("லோ", "§Ä¡");
        unicodeString = unicodeString.replace("லொ", "¦Ä¡");
        unicodeString = unicodeString.replace("லா", "Ä¡");
        unicodeString = unicodeString.replace("லி", "Ä¢");
        unicodeString = unicodeString.replace("லீ", "Ä£");
        unicodeString = unicodeString.replace("லு", "Ö");
        unicodeString = unicodeString.replace("லூ", "æ");
        unicodeString = unicodeString.replace("லெ", "¦Ä");
        unicodeString = unicodeString.replace("லே", "§Ä");
        unicodeString = unicodeString.replace("லை", "¨Ä");
        unicodeString = unicodeString.replace("ல்", "ø");
        unicodeString = unicodeString.replace("ல", "Ä");


        unicodeString = unicodeString.replace("ளௌ", "¦Çª");
        unicodeString = unicodeString.replace("ளோ", "§Ç¡");
        unicodeString = unicodeString.replace("ளொ", "¦Ç¡");
        unicodeString = unicodeString.replace("ளா", "Ç¡");
        unicodeString = unicodeString.replace("ளி", "Ç¢");
        unicodeString = unicodeString.replace("ளீ", "Ç£");
        unicodeString = unicodeString.replace("ளு", "Ù");
        unicodeString = unicodeString.replace("ளூ", "é");
        unicodeString = unicodeString.replace("ளெ", "¦Ç");
        unicodeString = unicodeString.replace("ளே", "§Ç");
        unicodeString = unicodeString.replace("ளை", "¨Ç");
        unicodeString = unicodeString.replace("ள்", "û");
        unicodeString = unicodeString.replace("ள", "Ç");

        unicodeString = unicodeString.replace("வௌ", "¦Åª");
        unicodeString = unicodeString.replace("வோ", "§Å¡");
        unicodeString = unicodeString.replace("வொ", "¦Å¡");
        unicodeString = unicodeString.replace("வா", "Å¡");
        unicodeString = unicodeString.replace("வி", "Å¢");
        unicodeString = unicodeString.replace("வீ", "Å£");
        unicodeString = unicodeString.replace("வு", "×");
        unicodeString = unicodeString.replace("வூ", "ç");
        unicodeString = unicodeString.replace("வெ", "¦Å");
        unicodeString = unicodeString.replace("வே", "§Å");
        unicodeString = unicodeString.replace("வை", "¨Å");
        unicodeString = unicodeString.replace("வ்", "ù");
        unicodeString = unicodeString.replace("வ", "Å");


        unicodeString = unicodeString.replace("ழௌ", "¦Æª");
        unicodeString = unicodeString.replace("ழோ", "§Æ¡");
        unicodeString = unicodeString.replace("ழொ", "¦Æ¡");
        unicodeString = unicodeString.replace("ழா", "Æ¡");
        unicodeString = unicodeString.replace("ழி", "Æ¢");
        unicodeString = unicodeString.replace("ழீ", "Æ£");
        unicodeString = unicodeString.replace("ழு", "Ø");
        unicodeString = unicodeString.replace("ழூ", "è");
        unicodeString = unicodeString.replace("ழெ", "¦Æ");
        unicodeString = unicodeString.replace("ழே", "§Æ");
        unicodeString = unicodeString.replace("ழை", "¨Æ");
        unicodeString = unicodeString.replace("ழ்", "ú");
        unicodeString = unicodeString.replace("ழ", "Æ");

        unicodeString = unicodeString.replace("றௌ", "¦Èª");
        unicodeString = unicodeString.replace("றோ", "§È¡");
        unicodeString = unicodeString.replace("றொ", "¦È¡");
        unicodeString = unicodeString.replace("றா", "È¡");
        unicodeString = unicodeString.replace("றி", "È¢");
        unicodeString = unicodeString.replace("றீ", "È£");
        unicodeString = unicodeString.replace("று", "Ú");
        unicodeString = unicodeString.replace("றூ", "ê");
        unicodeString = unicodeString.replace("றெ", "¦È");
        unicodeString = unicodeString.replace("றே", "§È");
        unicodeString = unicodeString.replace("றை", "¨È");
        unicodeString = unicodeString.replace("ற்", "ü");
        unicodeString = unicodeString.replace("ற", "È");


        unicodeString = unicodeString.replace("ஹௌ", "¦ª");
        unicodeString = unicodeString.replace("ஹோ", "§¡");
        unicodeString = unicodeString.replace("ஹொ", "¦¡");
        unicodeString = unicodeString.replace("ஹா", "†¡");
        unicodeString = unicodeString.replace("ஹி", "¢");
        unicodeString = unicodeString.replace("ஹீ", "£");
        unicodeString = unicodeString.replace("ஹு", "¤");
        unicodeString = unicodeString.replace("ஹூ", "¥");
        unicodeString = unicodeString.replace("ஹெ", "¦");
        unicodeString = unicodeString.replace("ஹே", "§");
        unicodeString = unicodeString.replace("ஹை", "¨");
        unicodeString = unicodeString.replace("ஹ்", "‹ ");
        unicodeString = unicodeString.replace("ஹ", "†");


        unicodeString = unicodeString.replace("ஷௌ", "¦„ª");
        unicodeString = unicodeString.replace("ஷோ", "§„¡");
        unicodeString = unicodeString.replace("ஷொ", "¦„¡");
        unicodeString = unicodeString.replace("ஷா", "„¡");
        unicodeString = unicodeString.replace("ஷி", "„¢");
        unicodeString = unicodeString.replace("ஷீ", "„£");
        unicodeString = unicodeString.replace("ஷு", "„¤");
        unicodeString = unicodeString.replace("ஷூ", "„¥");
        unicodeString = unicodeString.replace("ஷெ", "¦„");
        unicodeString = unicodeString.replace("ஷே", "§„");
        unicodeString = unicodeString.replace("ஷை", "¨„");
        unicodeString = unicodeString.replace("ஷ்", "‰");
        unicodeString = unicodeString.replace("ஷ", "„");


        unicodeString = unicodeString.replace("ஸௌ", "¦…ª");
        unicodeString = unicodeString.replace("ஸோ", "§…¡");
        unicodeString = unicodeString.replace("ஸொ", "¦…¡");
        unicodeString = unicodeString.replace("ஸா", "…¡");
        unicodeString = unicodeString.replace("ஸி", "…¢");
        unicodeString = unicodeString.replace("ஸீ", "…£");
        unicodeString = unicodeString.replace("ஸு", "…¤");
        unicodeString = unicodeString.replace("ஸூ", "…¥");
        unicodeString = unicodeString.replace("ஸெ", "¦…");
        unicodeString = unicodeString.replace("ஸே", "§…");
        unicodeString = unicodeString.replace("ஸை", "¨…");
        unicodeString = unicodeString.replace("ஸ்", "Š");
        unicodeString = unicodeString.replace("ஸ", "…");


        unicodeString = unicodeString.replace("அ", "«");
        unicodeString = unicodeString.replace("ஆ", "¬");
        unicodeString = unicodeString.replace("இ", "þ");
        unicodeString = unicodeString.replace("ஈ", "®");
        unicodeString = unicodeString.replace("உ", "¯");
        unicodeString = unicodeString.replace("ஊ", "°");
        unicodeString = unicodeString.replace("எ", "±");
        unicodeString = unicodeString.replace("ஏ", "²");
        unicodeString = unicodeString.replace("ஐ", "³");
        unicodeString = unicodeString.replace("ஒ", "´");
        unicodeString = unicodeString.replace("ஓ", "µ");
        unicodeString = unicodeString.replace("ஔ", "¶");
        unicodeString = unicodeString.replace("ஃ", "·");
        unicodeString = unicodeString.replace("ஸ்ரீ", "");
        unicodeString = unicodeString.replace("‘", "‘");

        unicodeString = unicodeString.replace("௧", "");
        unicodeString = unicodeString.replace("௨", "");
        unicodeString = unicodeString.replace("௩", "");
        unicodeString = unicodeString.replace("௪", "");
        unicodeString = unicodeString.replace("௫", "");
        unicodeString = unicodeString.replace("௰", "");
        unicodeString = unicodeString.replace("௱", "");
        unicodeString = unicodeString.replace("௲", "Ÿ");
        unicodeString = unicodeString.replace("௭", "–");
        unicodeString = unicodeString.replace("௮", "—");
        unicodeString = unicodeString.replace("௯", "˜");
        unicodeString = unicodeString.replace("௲", "Ÿ");
        unicodeString = unicodeString.replace("௭", "–");

        return unicodeString;
    }

    public static String toMocha(String unicodeString) {
        if (unicodeString == null) return "";
        unicodeString = unicodeString.replaceAll("ஸ்ரீ", "஘");

        unicodeString = unicodeString.replaceAll("கு", "஢");
        unicodeString = unicodeString.replaceAll("கூ", "஌");
        unicodeString = unicodeString.replaceAll("ஙு", "஖");
        unicodeString = unicodeString.replaceAll("ஙூ", "஖");
        unicodeString = unicodeString.replaceAll("சு", "௒");
        unicodeString = unicodeString.replaceAll("சூ", "௒஗");
        unicodeString = unicodeString.replaceAll("ஞு", "஖");
        unicodeString = unicodeString.replaceAll("ஞூ", "஖");
        unicodeString = unicodeString.replaceAll("டு", "஬");
        unicodeString = unicodeString.replaceAll("டூ", "௅");
        unicodeString = unicodeString.replaceAll("ணு", "௄");
        unicodeString = unicodeString.replaceAll("ணூ", "௄௑");
        unicodeString = unicodeString.replaceAll("து", "஧");
        unicodeString = unicodeString.replaceAll("தூ", "஧௑");
        unicodeString = unicodeString.replaceAll("நு", "஡");
        unicodeString = unicodeString.replaceAll("நூ", "஡௑");
        unicodeString = unicodeString.replaceAll("னு", "஠");
        unicodeString = unicodeString.replaceAll("னூ", "஠௑");
        unicodeString = unicodeString.replaceAll("பு", "஥");
        unicodeString = unicodeString.replaceAll("பூ", "஥஗");
        unicodeString = unicodeString.replaceAll("மு", "஫");
        unicodeString = unicodeString.replaceAll("மூ", "஍");
        unicodeString = unicodeString.replaceAll("யு", "஛");
        unicodeString = unicodeString.replaceAll("யூ", "஛஗");
        unicodeString = unicodeString.replaceAll("ரு", "஼");
        unicodeString = unicodeString.replaceAll("ரூ", "஑");
        unicodeString = unicodeString.replaceAll("லு", "௃");
        unicodeString = unicodeString.replaceAll("லூ", "௃௑");
        unicodeString = unicodeString.replaceAll("ளு", "஺");
        unicodeString = unicodeString.replaceAll("ளூ", "஺௓");
        unicodeString = unicodeString.replaceAll("வு", "஻");
        unicodeString = unicodeString.replaceAll("வூ", "஻஗");
        unicodeString = unicodeString.replaceAll("ழு", "஭");
        unicodeString = unicodeString.replaceAll("ழூ", "஖");
        unicodeString = unicodeString.replaceAll("று", "஽");
        unicodeString = unicodeString.replaceAll("றூ", "஽௑");
        unicodeString = unicodeString.replaceAll("டி", "௎");
        unicodeString = unicodeString.replaceAll("டீ", "஝");

        unicodeString = unicodeString.replaceAll("(.)ௌ", "௖$1௛");
        unicodeString = unicodeString.replaceAll("(.)ோ", "௘$1௓");
        unicodeString = unicodeString.replaceAll("(.)ொ", "௖$1௓");
        unicodeString = unicodeString.replaceAll("(.)ெ", "௖$1");
        unicodeString = unicodeString.replaceAll("(.)ே", "௘$1");
        unicodeString = unicodeString.replaceAll("(.)ை", "௙$1");

        unicodeString = unicodeString.replaceAll("ா", "௓");
        unicodeString = unicodeString.replaceAll("ி", "௔");
        unicodeString = unicodeString.replaceAll("ீ", "௕");
        unicodeString = unicodeString.replaceAll("ு", "௏");
        unicodeString = unicodeString.replaceAll("ூ", "௉");
        unicodeString = unicodeString.replaceAll("்", "௚");
        return unicodeString.replaceAll("◌", "໻");
    }

}
